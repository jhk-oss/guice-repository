package com.nordstrom.ecom.test.repository.customerOrder;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<CustomerOrderAddresses.CustomerOrderAddress, Long> {


}
