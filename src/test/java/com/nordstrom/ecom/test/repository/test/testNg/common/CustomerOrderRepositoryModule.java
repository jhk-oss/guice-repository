package com.nordstrom.ecom.test.repository.test.testNg.common;

import com.google.code.guice.repository.configuration.JpaRepositoryModule;
import com.google.code.guice.repository.configuration.RepositoryBinder;
import com.google.inject.AbstractModule;
import com.nordstrom.ecom.test.repository.customerOrder.CustomerOrderRepository;
import com.nordstrom.ecom.test.repository.customerOrder.CustomerOrderRepositoryImpl;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by james_000 on 11/9/2015.
 */
public class CustomerOrderRepositoryModule extends AbstractModule {
    @Override
    protected void configure() {
        install(new JpaRepositoryModule() {
            @Override
            protected void bindRepositories(RepositoryBinder binder) {
                binder.bind(CustomerOrderRepository.class).withCustomImplementation(CustomerOrderRepositoryImpl.class).to("test-hsqldb");
            }

            @Override
            protected Map<String, Object> getAdditionalEMFProperties(String persistenceUnitName) {
                Map<String, Object> properties = new HashMap<String, Object>();
                if ("test-hsqldb".equals(persistenceUnitName)){
                    properties.put("jpaDialect", new HibernateJpaDialect());
                    return properties;

                }
                return properties;
            }

            @Override
            protected Collection<String> getPersistenceUnitsNames() {
                return Arrays.asList("test-hsqldb");
            }
        });
//        new JpaRepositoryModule("test-hsqldb") {
//            @Override
//            protected void bindRepositories(RepositoryBinder binder) {
//                binder.bind(CustomerOrderRepository.class).to("test-hsqldb");
//            }
//        });
    }
}
