/*
 * Copyright (C) 2012 the original author or authors.
 * See the notice.md file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nordstrom.ecom.test.repository.test.testNg;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.nordstrom.ecom.test.repository.simple.Account;
import com.nordstrom.ecom.test.repository.simple.AccountRepository;
import com.nordstrom.ecom.test.repository.test.testNg.common.AccountRepositoryModule;
import org.testng.Assert;
import org.testng.annotations.Guice;
import org.testng.annotations.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;


@Guice(modules= AccountRepositoryModule.class)
public class AccountRepositoryTest {

    public AccountRepositoryTest() {

//        AccountRepositoryModule module = new AccountRepositoryModule();
//        Injector injector = com.google.inject.Guice.createInjector(module);
//
//        accountRepository = injector.getInstance(AccountRepository.class);
    }
    /*===========================================[ INSTANCE VARIABLES ]===========*/


    @Inject public AccountRepository accountRepository;

    /*===========================================[ CLASS METHODS ]================*/

    @Test
    public void testRepo() throws Exception {
        List<Account> accounts = new LinkedList<Account>();
        int count = 10;
        for (int i = 0; i < count; i++) {
            accounts.add(new Account(UUID.randomUUID().toString(), String.valueOf(i)));
        }

            try {
                accountRepository.save(accounts);
            } catch (Exception e) {
        }
        Assert.assertEquals(count, accountRepository.count(), "Invalid repository size");
        Assert.assertNotNull(accountRepository.findAccountByUuid(accounts.get(0).getUuid()), "Account not found by UUID");
    }
}