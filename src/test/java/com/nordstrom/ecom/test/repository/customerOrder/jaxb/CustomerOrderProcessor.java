package com.nordstrom.ecom.test.repository.customerOrder.jaxb;

import com.nordstrom.ecom.test.repository.customerOrder.CustomerOrder;
import com.nordstrom.ecom.test.repository.customerOrder.ObjectFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CustomerOrderProcessor {

    private static JAXBContext jaxbContext;
    private static String targetXmlPath = null;
    //            = "src/main/java/com/nordstrom/ecom/test/repository/entities/customerOrder/CustomerOrder.xml";
    private final Unmarshaller unmarshaller;

    public CustomerOrderProcessor() throws JAXBException {

        jaxbContext = JAXBContext.newInstance(
                com.nordstrom.ecom.test.repository.customerOrder.ObjectFactory.class);
        new ObjectFactory();
        unmarshaller = jaxbContext.createUnmarshaller();
    }

    static String readFile(String path, Charset encoding)
            throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    public CustomerOrder generate(String targetXmlPath) throws JAXBException, ClassNotFoundException, IOException {
        this.targetXmlPath = targetXmlPath;

        final Object object = unmarshaller.unmarshal(new File(targetXmlPath));

        // http://stackoverflow.com/questions/707084/class-cast-exception-when-trying-to-unmarshall-xml
        JAXBContext jaxbContext = JAXBContext.newInstance(Class.forName(
                "com.nordstrom.ecom.test.repository.customerOrder.CustomerOrder"));
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        String str = readFile(targetXmlPath, Charset.defaultCharset());
        Object schemaObject = JAXBIntrospector.getValue(unmarshaller.unmarshal(new ByteArrayInputStream(str.getBytes())));

        CustomerOrder customerOrder = (CustomerOrder) schemaObject;

//        @SuppressWarnings("unchecked")
//        final CustomerOrder customerOrder = ((JAXBElement<CustomerOrder>) object).getValue();

        return customerOrder;
    }

}
