package com.nordstrom.ecom.test.repository.test.testNg.common;

import com.google.code.guice.repository.configuration.JpaRepositoryModule;
import com.google.code.guice.repository.configuration.RepositoryBinder;
import com.google.inject.AbstractModule;
import com.nordstrom.ecom.test.repository.simple.AccountRepository;

/**
 * Created by james_000 on 11/9/2015.
 */
public class AccountRepositoryModule extends AbstractModule {
    @Override
    protected void configure() {
        install(new JpaRepositoryModule("test-h1") {
            @Override
            protected void bindRepositories(RepositoryBinder binder) {
                binder.bind(AccountRepository.class).to("test-h1");
            }
        });
    }
}
