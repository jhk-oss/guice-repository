/*
 * Copyright (C) 2012 the original author or authors.
 * See the notice.md file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nordstrom.ecom.test.repository.customerOrder;

import com.google.code.guice.repository.SimpleBatchStoreJpaRepository;
import com.nordstrom.ecom.test.repository.customerOrder.jaxb.CustomerOrderProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CustomerOrderRepositoryImpl
        extends SimpleBatchStoreJpaRepository<CustomerOrder,Long>
        implements CustomerOrderRepository {

	/*===========================================[ STATIC VARIABLES ]=============*/

    private static final Logger logger = LoggerFactory.getLogger(CustomerOrderRepositoryImpl.class);

	/*===========================================[ INSTANCE VARIABLES ]===========*/

    @Inject
    private EntityManager entityManager;



	/*===========================================[ CONSTRUCTORS ]=================*/

    public CustomerOrderRepositoryImpl(Class<CustomerOrder> domainClass, EntityManager em) {
        super(domainClass, em);
    }

    /*===========================================[ INTERFACE METHODS ]============*/

    public void saveFromXml(List<String> xmlPath) throws JAXBException {
        CustomerOrderProcessor processor = new CustomerOrderProcessor();
        List<CustomerOrder> customerOrders = new ArrayList<CustomerOrder>();

        try {

            for (String path : xmlPath) {
                customerOrders.add(processor.generate(path));
            }

            for (CustomerOrder co : customerOrders) {
                super.saveAndFlush(co);
//                entityManager.getTransaction().begin();
//                entityManager.persist(co);
//                entityManager.getTransaction().commit();
            }
//            entityManager.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }


    @Override
    public List<CustomerOrder.CustomerOrderCustomerIDs.CustomerOrderCustomerID> findAllIds() {
        return null;
    }
}
