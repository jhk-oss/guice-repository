package com.nordstrom.ecom.test.repository.customerOrder;

import org.jvnet.jaxb2_commons.lang.*;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;

import javax.persistence.*;
import javax.xml.bind.annotation.*;

/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomerOrderAddress">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                 &lt;attribute name="AddressLine1" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="AddressLine2" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="AddressXref" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="City" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="Country" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="DayPhone" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="EmailAddress" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="LastName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="StateProvince" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "customerOrderAddress"
})
@Entity(name = "CustomerOrder$CustomerOrderAddresses")
@Table(name = "CUSTOMER_ORDER_ADDRESSES")
@Inheritance(strategy = InheritanceType.JOINED)
public class CustomerOrderAddresses
    implements Equals, HashCode
{

    @XmlElement(name = "CustomerOrderAddress", required = true)
    protected CustomerOrderAddress customerOrderAddress;
    @XmlAttribute(name = "Hjid")
    protected Long hjid;

    /**
     * Gets the value of the customerOrderAddress property.
     *
     * @return
     *     possible object is
     *     {@link CustomerOrderAddress }
     *
     */
    @ManyToOne(targetEntity = CustomerOrderAddress.class, cascade = {
        CascadeType.ALL
    })
    @JoinColumn(name = "CUSTOMER_ORDER_ADDRESS_CUSTO_0")
    public CustomerOrderAddress getCustomerOrderAddress() {
        return customerOrderAddress;
    }

    /**
     * Sets the value of the customerOrderAddress property.
     *
     * @param value
     *     allowed object is
     *     {@link CustomerOrderAddress }
     *
     */
    public void setCustomerOrderAddress(CustomerOrderAddress value) {
        this.customerOrderAddress = value;
    }

    @Transient
    public boolean isSetCustomerOrderAddress() {
        return (this.customerOrderAddress!= null);
    }

    /**
     * Gets the value of the hjid property.
     *
     * @return
     *     possible object is
     *     {@link Long }
     *
     */
    @Id
    @Column(name = "HJID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getHjid() {
        return hjid;
    }

    /**
     * Sets the value of the hjid property.
     *
     * @param value
     *     allowed object is
     *     {@link Long }
     *
     */
    public void setHjid(Long value) {
        this.hjid = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof CustomerOrderAddresses)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final CustomerOrderAddresses that = ((CustomerOrderAddresses) object);
        {
            CustomerOrderAddress lhsCustomerOrderAddress;
            lhsCustomerOrderAddress = this.getCustomerOrderAddress();
            CustomerOrderAddress rhsCustomerOrderAddress;
            rhsCustomerOrderAddress = that.getCustomerOrderAddress();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "customerOrderAddress", lhsCustomerOrderAddress), LocatorUtils.property(thatLocator, "customerOrderAddress", rhsCustomerOrderAddress), lhsCustomerOrderAddress, rhsCustomerOrderAddress)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            CustomerOrderAddress theCustomerOrderAddress;
            theCustomerOrderAddress = this.getCustomerOrderAddress();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "customerOrderAddress", theCustomerOrderAddress), currentHashCode, theCustomerOrderAddress);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *       &lt;attribute name="AddressLine1" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="AddressLine2" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="AddressXref" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="City" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="Country" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="DayPhone" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="EmailAddress" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="LastName" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="StateProvince" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    @Entity(name = "com.nordstrom.ecom.test.repository.data.CustomerOrder$CustomerOrderAddresses$CustomerOrderAddress")
    @Table(name = "CUSTOMER_ORDER_ADDRESS")
    @Inheritance(strategy = InheritanceType.JOINED)
    public static class CustomerOrderAddress
        implements Equals, HashCode
    {

        @XmlValue
        protected String value;
        @XmlAttribute(name = "AddressLine1")
        protected String addressLine1;
        @XmlAttribute(name = "AddressLine2")
        protected String addressLine2;
        @XmlAttribute(name = "AddressXref")
        protected String addressXref;
        @XmlAttribute(name = "City")
        protected String city;
        @XmlAttribute(name = "Country")
        protected String country;
        @XmlAttribute(name = "DayPhone")
        protected String dayPhone;
        @XmlAttribute(name = "EmailAddress")
        protected String emailAddress;
        @XmlAttribute(name = "FirstName")
        protected String firstName;
        @XmlAttribute(name = "LastName")
        protected String lastName;
        @XmlAttribute(name = "MiddleName")
        protected String middleName;
        @XmlAttribute(name = "PostalCode")
        protected String postalCode;
        @XmlAttribute(name = "StateProvince")
        protected String stateProvince;
        @XmlAttribute(name = "Hjid")
        protected Long hjid;

        /**
         * Gets the value of the value property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        @Basic
        @Column(name = "VALUE_", length = 255)
        public String getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setValue(String value) {
            this.value = value;
        }

        @Transient
        public boolean isSetValue() {
            return (this.value!= null);
        }

        /**
         * Gets the value of the addressLine1 property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        @Basic
        @Column(name = "ADDRESS_LINE_1", length = 255)
        public String getAddressLine1() {
            return addressLine1;
        }

        /**
         * Sets the value of the addressLine1 property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAddressLine1(String value) {
            this.addressLine1 = value;
        }

        @Transient
        public boolean isSetAddressLine1() {
            return (this.addressLine1 != null);
        }

        /**
         * Gets the value of the addressLine2 property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        @Basic
        @Column(name = "ADDRESS_LINE_2", length = 255)
        public String getAddressLine2() {
            return addressLine2;
        }

        /**
         * Sets the value of the addressLine2 property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAddressLine2(String value) {
            this.addressLine2 = value;
        }

        @Transient
        public boolean isSetAddressLine2() {
            return (this.addressLine2 != null);
        }

        /**
         * Gets the value of the addressXref property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        @Basic
        @Column(name = "ADDRESS_XREF", length = 255)
        public String getAddressXref() {
            return addressXref;
        }

        /**
         * Sets the value of the addressXref property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setAddressXref(String value) {
            this.addressXref = value;
        }

        @Transient
        public boolean isSetAddressXref() {
            return (this.addressXref!= null);
        }

        /**
         * Gets the value of the city property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        @Basic
        @Column(name = "CITY", length = 255)
        public String getCity() {
            return city;
        }

        /**
         * Sets the value of the city property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setCity(String value) {
            this.city = value;
        }

        @Transient
        public boolean isSetCity() {
            return (this.city!= null);
        }

        /**
         * Gets the value of the country property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        @Basic
        @Column(name = "COUNTRY", length = 255)
        public String getCountry() {
            return country;
        }

        /**
         * Sets the value of the country property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setCountry(String value) {
            this.country = value;
        }

        @Transient
        public boolean isSetCountry() {
            return (this.country!= null);
        }

        /**
         * Gets the value of the dayPhone property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        @Basic
        @Column(name = "DAY_PHONE", length = 255)
        public String getDayPhone() {
            return dayPhone;
        }

        /**
         * Sets the value of the dayPhone property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setDayPhone(String value) {
            this.dayPhone = value;
        }

        @Transient
        public boolean isSetDayPhone() {
            return (this.dayPhone!= null);
        }

        /**
         * Gets the value of the emailAddress property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        @Basic
        @Column(name = "EMAIL_ADDRESS", length = 255)
        public String getEmailAddress() {
            return emailAddress;
        }

        /**
         * Sets the value of the emailAddress property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setEmailAddress(String value) {
            this.emailAddress = value;
        }

        @Transient
        public boolean isSetEmailAddress() {
            return (this.emailAddress!= null);
        }

        /**
         * Gets the value of the firstName property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        @Basic
        @Column(name = "FIRST_NAME", length = 255)
        public String getFirstName() {
            return firstName;
        }

        /**
         * Sets the value of the firstName property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setFirstName(String value) {
            this.firstName = value;
        }

        @Transient
        public boolean isSetFirstName() {
            return (this.firstName!= null);
        }

        /**
         * Gets the value of the lastName property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        @Basic
        @Column(name = "LAST_NAME", length = 255)
        public String getLastName() {
            return lastName;
        }

        /**
         * Sets the value of the lastName property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setLastName(String value) {
            this.lastName = value;
        }

        @Transient
        public boolean isSetLastName() {
            return (this.lastName!= null);
        }

        /**
         * Gets the value of the middleName property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        @Basic
        @Column(name = "MIDDLE_NAME", length = 255)
        public String getMiddleName() {
            return middleName;
        }

        /**
         * Sets the value of the middleName property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setMiddleName(String value) {
            this.middleName = value;
        }

        @Transient
        public boolean isSetMiddleName() {
            return (this.middleName!= null);
        }

        /**
         * Gets the value of the postalCode property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        @Basic
        @Column(name = "POSTAL_CODE", length = 255)
        public String getPostalCode() {
            return postalCode;
        }

        /**
         * Sets the value of the postalCode property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setPostalCode(String value) {
            this.postalCode = value;
        }

        @Transient
        public boolean isSetPostalCode() {
            return (this.postalCode!= null);
        }

        /**
         * Gets the value of the stateProvince property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        @Basic
        @Column(name = "STATE_PROVINCE", length = 255)
        public String getStateProvince() {
            return stateProvince;
        }

        /**
         * Sets the value of the stateProvince property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setStateProvince(String value) {
            this.stateProvince = value;
        }

        @Transient
        public boolean isSetStateProvince() {
            return (this.stateProvince!= null);
        }

        /**
         * Gets the value of the hjid property.
         *
         * @return
         *     possible object is
         *     {@link Long }
         *
         */
        @Id
        @Column(name = "HJID")
        @GeneratedValue(strategy = GenerationType.AUTO)
        public Long getHjid() {
            return hjid;
        }

        /**
         * Sets the value of the hjid property.
         *
         * @param value
         *     allowed object is
         *     {@link Long }
         *
         */
        public void setHjid(Long value) {
            this.hjid = value;
        }

        public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
            if (!(object instanceof CustomerOrderAddress)) {
                return false;
            }
            if (this == object) {
                return true;
            }
            final CustomerOrderAddress that = ((CustomerOrderAddress) object);
            {
                String lhsValue;
                lhsValue = this.getValue();
                String rhsValue;
                rhsValue = that.getValue();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "value", lhsValue), LocatorUtils.property(thatLocator, "value", rhsValue), lhsValue, rhsValue)) {
                    return false;
                }
            }
            {
                String lhsAddressLine1;
                lhsAddressLine1 = this.getAddressLine1();
                String rhsAddressLine1;
                rhsAddressLine1 = that.getAddressLine1();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "addressLine1", lhsAddressLine1), LocatorUtils.property(thatLocator, "addressLine1", rhsAddressLine1), lhsAddressLine1, rhsAddressLine1)) {
                    return false;
                }
            }
            {
                String lhsAddressLine2;
                lhsAddressLine2 = this.getAddressLine2();
                String rhsAddressLine2;
                rhsAddressLine2 = that.getAddressLine2();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "addressLine2", lhsAddressLine2), LocatorUtils.property(thatLocator, "addressLine2", rhsAddressLine2), lhsAddressLine2, rhsAddressLine2)) {
                    return false;
                }
            }
            {
                String lhsAddressXref;
                lhsAddressXref = this.getAddressXref();
                String rhsAddressXref;
                rhsAddressXref = that.getAddressXref();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "addressXref", lhsAddressXref), LocatorUtils.property(thatLocator, "addressXref", rhsAddressXref), lhsAddressXref, rhsAddressXref)) {
                    return false;
                }
            }
            {
                String lhsCity;
                lhsCity = this.getCity();
                String rhsCity;
                rhsCity = that.getCity();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "city", lhsCity), LocatorUtils.property(thatLocator, "city", rhsCity), lhsCity, rhsCity)) {
                    return false;
                }
            }
            {
                String lhsCountry;
                lhsCountry = this.getCountry();
                String rhsCountry;
                rhsCountry = that.getCountry();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "country", lhsCountry), LocatorUtils.property(thatLocator, "country", rhsCountry), lhsCountry, rhsCountry)) {
                    return false;
                }
            }
            {
                String lhsDayPhone;
                lhsDayPhone = this.getDayPhone();
                String rhsDayPhone;
                rhsDayPhone = that.getDayPhone();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "dayPhone", lhsDayPhone), LocatorUtils.property(thatLocator, "dayPhone", rhsDayPhone), lhsDayPhone, rhsDayPhone)) {
                    return false;
                }
            }
            {
                String lhsEmailAddress;
                lhsEmailAddress = this.getEmailAddress();
                String rhsEmailAddress;
                rhsEmailAddress = that.getEmailAddress();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "emailAddress", lhsEmailAddress), LocatorUtils.property(thatLocator, "emailAddress", rhsEmailAddress), lhsEmailAddress, rhsEmailAddress)) {
                    return false;
                }
            }
            {
                String lhsFirstName;
                lhsFirstName = this.getFirstName();
                String rhsFirstName;
                rhsFirstName = that.getFirstName();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "firstName", lhsFirstName), LocatorUtils.property(thatLocator, "firstName", rhsFirstName), lhsFirstName, rhsFirstName)) {
                    return false;
                }
            }
            {
                String lhsLastName;
                lhsLastName = this.getLastName();
                String rhsLastName;
                rhsLastName = that.getLastName();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "lastName", lhsLastName), LocatorUtils.property(thatLocator, "lastName", rhsLastName), lhsLastName, rhsLastName)) {
                    return false;
                }
            }
            {
                String lhsMiddleName;
                lhsMiddleName = this.getMiddleName();
                String rhsMiddleName;
                rhsMiddleName = that.getMiddleName();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "middleName", lhsMiddleName), LocatorUtils.property(thatLocator, "middleName", rhsMiddleName), lhsMiddleName, rhsMiddleName)) {
                    return false;
                }
            }
            {
                String lhsPostalCode;
                lhsPostalCode = this.getPostalCode();
                String rhsPostalCode;
                rhsPostalCode = that.getPostalCode();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "postalCode", lhsPostalCode), LocatorUtils.property(thatLocator, "postalCode", rhsPostalCode), lhsPostalCode, rhsPostalCode)) {
                    return false;
                }
            }
            {
                String lhsStateProvince;
                lhsStateProvince = this.getStateProvince();
                String rhsStateProvince;
                rhsStateProvince = that.getStateProvince();
                if (!strategy.equals(LocatorUtils.property(thisLocator, "stateProvince", lhsStateProvince), LocatorUtils.property(thatLocator, "stateProvince", rhsStateProvince), lhsStateProvince, rhsStateProvince)) {
                    return false;
                }
            }
            return true;
        }

        public boolean equals(Object object) {
            final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
            return equals(null, null, object, strategy);
        }

        public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
            int currentHashCode = 1;
            {
                String theValue;
                theValue = this.getValue();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "value", theValue), currentHashCode, theValue);
            }
            {
                String theAddressLine1;
                theAddressLine1 = this.getAddressLine1();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "addressLine1", theAddressLine1), currentHashCode, theAddressLine1);
            }
            {
                String theAddressLine2;
                theAddressLine2 = this.getAddressLine2();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "addressLine2", theAddressLine2), currentHashCode, theAddressLine2);
            }
            {
                String theAddressXref;
                theAddressXref = this.getAddressXref();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "addressXref", theAddressXref), currentHashCode, theAddressXref);
            }
            {
                String theCity;
                theCity = this.getCity();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "city", theCity), currentHashCode, theCity);
            }
            {
                String theCountry;
                theCountry = this.getCountry();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "country", theCountry), currentHashCode, theCountry);
            }
            {
                String theDayPhone;
                theDayPhone = this.getDayPhone();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "dayPhone", theDayPhone), currentHashCode, theDayPhone);
            }
            {
                String theEmailAddress;
                theEmailAddress = this.getEmailAddress();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "emailAddress", theEmailAddress), currentHashCode, theEmailAddress);
            }
            {
                String theFirstName;
                theFirstName = this.getFirstName();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "firstName", theFirstName), currentHashCode, theFirstName);
            }
            {
                String theLastName;
                theLastName = this.getLastName();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "lastName", theLastName), currentHashCode, theLastName);
            }
            {
                String theMiddleName;
                theMiddleName = this.getMiddleName();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "middleName", theMiddleName), currentHashCode, theMiddleName);
            }
            {
                String thePostalCode;
                thePostalCode = this.getPostalCode();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "postalCode", thePostalCode), currentHashCode, thePostalCode);
            }
            {
                String theStateProvince;
                theStateProvince = this.getStateProvince();
                currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "stateProvince", theStateProvince), currentHashCode, theStateProvince);
            }
            return currentHashCode;
        }

        public int hashCode() {
            final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
            return this.hashCode(null, strategy);
        }

    }

}
